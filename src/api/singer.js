import { get } from '../service/request'

let API = {
  /**
   * 获取歌手列表, 以及排序过的, 且列表第一个元素是'热'
   */
  getSingerList () {
    return get('/api/getSingerList')
  },

  /**
   * 获取歌手详情信息
   * @param singer
   * @return {Promise<* | void>}
   */
  getSingerDetail (singer) {
    return get('/api/getSingerDetail', {
      mid: singer.mid
    })
  }
}

export default API
