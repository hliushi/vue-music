import { get } from '../service/request'

let API = {

  /**
   * 批量处理, 获取到歌手的能播放歌曲地址, 并且封装到歌手信息里面
   * @param songs  Promise对象
   * @return {Promise<unknown>|string}
   */
  processSongs (songs) {
    if (!songs.length) {
      // 原样返回
      return Promise.resolve(songs)
    }
    return get('/api/getSongsUrl', {
      mid: songs.map((song) => {
        return song.mid
      })
    }).then((res) => {
      // map的key/val -> 以歌曲的mid为key, 存储歌曲的URL为val
      const map = res.map
      return songs.map((song) => {
        song.url = map[song.mid]
        return song
      }).filter((song) => {
        // 过滤可以播放的歌曲url, 能播放的url一定一个vkey
        return song.url.indexOf('vkey') > -1
      })
    })
  }
}
export default API
