import axios from 'axios'

const ERR_OK = 0
const baseUrl = '/'

const service = axios.create({
  baseURL: baseUrl
})

export function get (url, params) {
  return service.get(url, {
    params
  }).then((res) => {
    const serverData = res.data
    if (serverData.code === ERR_OK) {
      return serverData.result
    }
  }).catch((err) => {
    console.log(err)
  })
}
