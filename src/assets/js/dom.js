export function addClass (el, className) {
  if (!el.classList.contains(className)) {
    // dom API 添加类名样式
    el.classList.add(className)
  }
}

export function removeClass (el, className) {
  // dom API 移除类名样式
  el.classList.remove(className)
}
