import { get } from '../service/request'

let API = {
  /**
   * 获取轮播图
   * @return {Promise<* | void>}
   */
  getRecommend () {
    return get('/api/getRecommend')
  }
}

export default API
