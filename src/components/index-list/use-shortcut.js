import { computed, ref } from 'vue'

export default function useShortcut (props, groupRef) {
  const ANCHOR_HEIGHT = 18
  const scrollRef = ref(null)
  const touch = {}

  const shortcutList = computed(() => {
    let arr = []

    // 两种方式都可以, 一个实用for循环, 一个实用
    // for (let i = 0; i < props.data.length; i++) {
    //   arr.push(props.data[i].title)
    // }
    arr = props.data.map(item => item.title)
    // console.log(arr)
    return arr
  })

  /**
   * touchStart事件: 当手指触摸屏幕时候触发, 即是已经有一个手指放在屏幕也会触发
   * @param e
   */
  function onShortcutTouchStart (e) {
    // console.log(e.target)
    // todo 会出现 NAN问题, touchmove快捷入口上下两个padding的时候, anchorIndex = NAN
    const anchorIndex = parseInt(e.target.dataset.index)
    touch.y1 = e.touches[0].pageY
    touch.anchorIndex = anchorIndex

    scrollTo(anchorIndex)
  }

  /**
   * touchMove事件: 当手指在屏幕上滑动的时候连续地触发
   * @param e
   */
  function onShortcutTouchMove (e) {
    touch.y2 = e.touches[0].pageY
    //    | 0 或0的意义是 向下取整的一种写法
    const delta = (touch.y2 - touch.y1) / ANCHOR_HEIGHT | 0
    const anchorIndex = touch.anchorIndex + delta

    scrollTo(anchorIndex)
  }

  function scrollTo (index) {

    // console.log(index)
    // 如果index是NAN, 则直接返回, 不会滑动
    if (isNaN(index)) {
      return
    }
    // todo ???
    index = Math.max(0, Math.min(shortcutList.value.length - 1, index))
    const targetEle = groupRef.value.children[index]

    // 通过ref引用拿到 Scroll 组件的内部的 scroll 变量
    const scroll = scrollRef.value.scroll
    // scrollToElement(el, time) 滚动到指定的目标元素
    scroll.scrollToElement(targetEle, 0)
  }

  return {
    shortcutList,
    onShortcutTouchStart,
    scrollRef,
    onShortcutTouchMove
  }
}
