// 导入BScroll的核心内容
import BScroll from '@better-scroll/core'
import Slide from '@better-scroll/slide'

import { onMounted, onUnmounted, ref } from 'vue'

BScroll.use(Slide)

export default function useSlider (wrapperRef) {
  // 定义两个响应式的普通数据结构类型的数据
  const slider = ref(null)
  const currentPageIndex = ref(0)

  onMounted(() => {
    // 由于wrapperRef是 ref 类别数据, 所以获取值 | 修改值, 都需要用到它的value属性
    // vue3.0 wrapperRef.value  ===  vue2.0  this.$refs.wrapperRef 通过value属性才能获取到DOM元素
    // 定义一个 sliderVal 变量缓存 slider.value, 简单来说, sliderVal === slider.value
    // 只不过, 把代码整理的更简短些
    const sliderVal = slider.value = new BScroll(wrapperRef.value, {
      click: true,
      scrollX: true,
      scrollY: false,
      momentum: false,
      bounce: false,
      probeType: 2,
      slide: true
    })

    /**
     * slideWillChange事件
     * 触发时机: slide 的 currentPage 值将要改变时
     */
    sliderVal.on('slideWillChange', (page) => {
      // page.pageX : 即将展示的横向页面的索引值, 下标从0开始
      currentPageIndex.value = page.pageX
    })
  })

  /**
   * 这里有必要销毁吗, vue会自动帮我们销毁吗?
   */
  onUnmounted(() => {
    slider.value.destroy()
  })

  return {
    slider,
    currentPageIndex
  }
}
