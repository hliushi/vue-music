// 导入BScroll的核心内容
import BScroll from '@better-scroll/core'
import ObserveDOM from '@better-scroll/observe-dom'
import { onMounted, onUnmounted, onActivated, onDeactivated, ref } from 'vue'

BScroll.use(ObserveDOM)

export default function useScroll (wrapperRef, options, emit) {
  // debugger
  const scroll = ref(null)

  onMounted(() => {
    scroll.value = new BScroll(wrapperRef.value, {
      // observeDOM 开启对 content 以及 content 子元素DOM改变的探测
      // 当插件被使用后, 当这些DOM元素发生改变时, 将会触发scroll的refresh方法
      observeDOM: true, // 开启 observe-dom 插件
      ...options        // click: true -->
    })
    if (options.probeType > 0) {
      scroll.value.on('scroll', (pos) => {
        // 事件派发 ?? todo
        emit('scroll', pos)
      })
    }
  })

  onUnmounted(() => {
    scroll.value.destroy()
  })

  return {
    scroll
  }
}
