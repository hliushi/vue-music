import { createApp } from 'vue'
import Loading from './Loading'
import { addClass, removeClass } from '../../../assets/js/dom'

// bug点: 注意这个 className 类名不需要加 .
const relativeCls = 'g-relative'

const loadingDirective = {
  /**
   * bug点, mounted 写成 mount
   * mounted 元素插入父DOM后调用
   */
  mounted (el, binding) {
    const app = createApp(Loading)
    // 创建一个动态 div 实例, 这是为什么?
    const instance = app.mount(document.createElement('div'))
    // 将 instance 缓存起来
    el.instance = instance

    // 优化: 设置title
    const title = binding.arg
    // 不等于空的一种判断方式
    if (typeof title !== 'undefined') {
      instance.setTitle(title)
    }

    if (binding.value) {
      append(el)
    }
  },
  /**
   * 一旦组件和子级被更新, 调用这个钩子
   * @param el        表示当前指令所绑定到的那个DOM元素
   * @param binding   v-loading="loading" -> loading值传递给 binding 参数, 使用binding.value拿到真正的值
   */
  updated (el, binding) {
    // 优化: 设置title
    // v-loading:[loadingText]="loading"   bind.arg = loadingText变量的值
    const title = binding.arg
    // 不等于空的一种判断方式
    if (typeof title !== 'undefined') {
      el.instance.setTitle(title)
    }

    // 更新方法时, 可以拿到oldValue
    if (binding.value !== binding.oldValue) {
      binding.value ? append(el) : remove(el)
    }
  }

}

function append (el) {
  // 优化 动态给el添加一个相对定位的样式
  const style = getComputedStyle(el)
  if (['absolute', 'fixed', 'relative'].indexOf(style.position) === -1) {
    addClass(el, relativeCls)
  }
  el.appendChild(el.instance.$el)
}

function remove (el) {
  removeClass(el, relativeCls)
  el.removeChild(el.instance.$el)
}

export default loadingDirective
