import { createRouter, createWebHashHistory } from 'vue-router'
import recommend from '../views/recommend'
import search from '../views/search'
import singer from '../views/singer'
import topList from '../views/topList'
import singerDetail from '../views/singer-detail'

const routes = [
  {
    path: '/',
    redirect: '/recommend'
  },
  {
    path: '/recommend',
    component: recommend
  },
  {
    path: '/search',
    component: search
  },
  {
    path: '/singer',
    component: singer,
    children: [
      {
        path: ':id',
        component: singerDetail
      }
    ]
  },
  {
    path: '/top-list',
    component: topList
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
