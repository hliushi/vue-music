import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 使用图片懒加载插件
import lazyPlugin from 'vue3-lazy'

import loadingDirective from './components/base/loading/directive'
import noResultDirective from './components/base/no-result/directive'

// 引入全局样式文件
import '@/assets/scss/index.scss'

createApp(App)
  .use(lazyPlugin, {
    // require -> webpack可以识别的
    loading: require('@/assets/images/default03.png'),
  })
  .directive('loading', loadingDirective)
  .directive('no-result', noResultDirective)
  .use(store)     // vue3使用插件的方式
  .use(router)
  .mount('#app')
